import './MenuItem.scss';

function MenuItem(props) {
    const { child } = props

    return (
        <div className="menu-item">
            {child}
        </div>
    )
}

export default MenuItem;