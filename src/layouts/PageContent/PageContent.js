import './PageContent.scss';

function PageContent() {
    return (
        <div className="page-content">
            <div className="splash-text">Our fruits & vegetables are <span>organic.</span></div>
            <div className="text"><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></div>
            <div className="button"><span className="button-text">DISCOVER ▶</span></div>
        </div>
    );
}

export default PageContent;
