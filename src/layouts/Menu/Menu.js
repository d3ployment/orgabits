import Dash from '../../components/Dash/Dash';
import MenuItem from '../../components/MenuItem/MenuItem';
import './Menu.scss';
import logo from '../../assets/icons/logo.png'
import cart from '../../assets/icons/cart.png'
import hamburger from '../../assets/icons/hamburger.png'
import search from '../../assets/icons/search.png'

function Menu() {
    return (
        <div class="menu">
            <MenuItem child={<img src={logo} alt="" />} />
            <MenuItem child={<img src={cart} alt="" />} />
            <MenuItem child={<Dash orientation="vertical" />} />
            <MenuItem child={<img src={hamburger} alt="" />} />
            <MenuItem child={<Dash orientation="vertical" />} />
            <MenuItem child={<img src={search} alt="" />} />
            <MenuItem child={<span className="orgabits"><span className="orga">ORGA</span>BITS</span>} />
        </div>
    );
}

export default Menu;
